﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ase_cyclingapp
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }
        static string[] getParam(string line)
        {
            return line.Split('=');
        }
        DateTime start;
        string date;
        List<Tuple<string, DateTime>> bolded = new List<Tuple<string, DateTime>>();
        public static DateTime startDate(string date)
        {
            //getting datetime

            int year = Convert.ToInt32(date.Substring(0, 4));
            int month = Convert.ToInt32(date.Substring(4, 2));
            int day = Convert.ToInt32(date.Substring(6, 2));

            DateTime start = new DateTime(year, month, day);
            return start;
        }
        private void Form3_Load(object sender, EventArgs e)
        {
            bool isParameter = false;
            Console.WriteLine("load");
            foreach (string file in Directory.EnumerateFiles("Data", "*.hrm"))
            {
                Console.WriteLine("for each");
                using (StreamReader sr = new StreamReader(file))
                {
                    Console.WriteLine("reader");
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (isParameter)
                        {
                            if (line.Length > 0)
                            {
                                string[] p = getParam(line);
                                if (p[1] != null)
                                {
                                    if (p[0] == "Date")
                                    {
                                        date = p[1];
                                        start = startDate(date);
                                        monthCalendar1.AddBoldedDate(start);
                                        bolded.Add(Tuple.Create(file, start));
                                    }
                                }
                            }
                        }
                        if (line.Length < 1 || line.Substring(0, 1) == "[") //substring of a string
                        {
                            isParameter = false;
                        }
                        if (line == "[Params]")
                        {
                            isParameter = true;
                        }
                    }
                }
            }
        }
        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            Console.WriteLine(e.Start);
            foreach (var b in bolded)
            {
                if (e.Start == b.Item2)
                {
                    Form app = new Form1(b.Item1); /// passing value from form 3 to form1
                    app.Show();
                    this.Hide();
                }

            }
        }
    }
}
